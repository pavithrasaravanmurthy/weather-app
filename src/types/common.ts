export type WeatherData = {
  coord: coord;
  weather: weather[];
  base: string;
  main: Main;
  wind: Wind;
  clouds: Clouds;
  dt: number;
  sys: Sys;
  timezone: number;
  id: number;
  name: string;
  cod: number;
};
export type WeatherForecast = {
  dt: number;
  main: Main;
  weather: weather[];
  clouds: Clouds;
  wind: Wind;
  visibility: number;
  pop: number;
  rain: Rain;
  sys: Sys;
  dt_txt: string;
};

export type weather = {
  id: number;
  main: string;
  description: string;
  icon: string;
};

export type coord = {
  lon: number;
  lat: number;
};
export type Main = {
  temp: number;
  feels_like: number;
  temp_min: number;
  temp_max: number;
  pressure: number;
  humidity: number;
  visibility?: number;
  sea_level?: number;
  grnd_level?: number;
  temp_kf?: number;
};

export type Wind = {
  speed: number;
  deg: number;
  gust: number;
};

export type Clouds = {
  all: number;
};

export type Sys = {
  type?: number;
  id?: number;
  country?: string;
  sunrise?: number;
  sunset?: number;
  pod?: string;
};
export type Rain = {
  "3h": number;
};
