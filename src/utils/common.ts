export function getCardinalDirection(angle: number) {
  const directions = [
    " N↑",
    " NE↗",
    " E→",
    " SE↘",
    " S↓",
    " SW↙",
    " W←",
    " NW↖",
  ];
  return directions[Math.round(angle / 45) % 8];
}

export function timeConverter(UNIX_timestamp: number) {
  var a = new Date(UNIX_timestamp * 1000);
  var months = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec",
  ];
  var month = months[a.getMonth()];
  var d = a.getDate();
  var hour = a.getHours();
  var min = a.getMinutes();
  var date = d + " " + month;
  var time = (hour < 10 ? "0" + hour : hour) + ":" + min;
  return [date, tConv24(time)];
}

export function tConv24(time24: string) {
  var ts = time24;
  var H = +ts.substr(0, 2);
  var h: number | string = H % 12 || 12;
  h = h < 10 ? "0" + h : h;
  var ampm = H < 12 ? " AM" : " PM";
  ts = h + ts.substr(2, 3) + ampm;
  return ts;
}
