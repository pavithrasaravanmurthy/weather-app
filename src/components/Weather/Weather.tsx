import React, { FormEvent, useEffect, useState } from "react";
import { WeatherData, WeatherForecast, weather } from "types/common";
import "./Weather.css";
import { getCardinalDirection, timeConverter } from "utils/common";

export default function Weather() {
  const [city, setCity] = useState("Chennai");
  const [currentWeather, setCurrentWeather] = useState<WeatherData>();
  const [weatherForecast, setWeatherForecast] = useState<WeatherForecast[]>();
  const [errors, setErrors] = useState<string[]>([]);
  const [unit, setUnit] = useState("metric");

  useEffect(() => {
    fetchWeather();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [unit]);

  const fetchWeather = async (e?: FormEvent) => {
    e?.preventDefault();
    setCurrentWeather(undefined);
    setWeatherForecast(undefined);
    setErrors([]);
    await fetch(
      `https://api.openweathermap.org/data/2.5/weather?q=${city}&APPID=f86349cf150ce45602f0e54f6962d33a&units=${unit}`
    )
      .then((res) => {
        if (res.ok) return res.json();
        return Promise.reject(res);
      })
      .then((data: WeatherData) => {
        fetch(
          `https://api.openweathermap.org/data/2.5/forecast?lat=${data.coord.lat}&lon=${data.coord.lon}&appid=f86349cf150ce45602f0e54f6962d33a&units=${unit}`
        )
          .then((res) => {
            if (res.ok) return res.json();
            return Promise.reject(res);
          })
          .then((data) => {
            setWeatherForecast(data.list);
          })
          .catch((err) => {
            err.json().then((e: any) => {
              setErrors((prev) => [...prev, e.message]);
              console.log(e);
            });
          });
        setCurrentWeather(data);
      })
      .catch((err) => {
        err.json().then((e: any) => {
          setErrors((prev) => [...prev, e.message]);
          console.log(e);
        });
      });
  };

  return (
    <div className="container">
      <form onSubmit={fetchWeather} className="search">
        <input
          className="input"
          placeholder="eg.Chennai"
          value={city}
          onChange={(e: any) => setCity(e.target.value)}
        />
        <button className="button" type="submit">
          Search
        </button>
      </form>

      {currentWeather && (
        <>
          <div>Results for {currentWeather.name}</div>
          <div className="current">
            <div className="status">
              <img
                src={`https://openweathermap.org/img/wn/${currentWeather.weather[0].icon}@2x.png`}
                alt=""
              />
              <div className="temperature">
                {currentWeather.main.temp.toFixed(0)}
                <span className="units">
                  <span
                    className={unit === "metric" ? "active" : ""}
                    onClick={() => setUnit("metric")}
                  >
                    °C
                  </span>
                  |
                  <span
                    className={unit === "imperial" ? "active" : ""}
                    onClick={() => setUnit("imperial")}
                  >
                    °F
                  </span>
                </span>
              </div>
              <div className="conditions">
                <ul className="list">
                  <li>Humidity: {currentWeather.main.humidity}%</li>
                  <li>
                    Wind: {currentWeather.wind.speed}mph
                    <span>{getCardinalDirection(currentWeather.wind.deg)}</span>
                  </li>
                  <li>Max temp: {currentWeather.main.temp_max.toFixed(0)}°c</li>
                  <li>
                    Min temp:{" "}
                    {(currentWeather.main.temp_min - 273.15).toFixed(0)}°c
                  </li>
                </ul>
              </div>
            </div>

            <div className="description">
              <div className="title">Weather </div>
              {timeConverter(currentWeather.dt).map((s) => (
                <span className="time"> {s}</span>
              ))}
              {currentWeather && (
                <div>
                  {currentWeather.weather.map(
                    (item: weather) =>
                      item.description[0].toUpperCase() +
                      item.description.substring(1)
                  )}
                </div>
              )}
            </div>
          </div>
        </>
      )}

      {weatherForecast && (
        <div className="forecast">
          {weatherForecast.map((item) => (
            <div className="shadowbox">
              <div className="text">
                {timeConverter(item.dt).map((s) => (
                  <div>{s}</div>
                ))}
              </div>
              <img
                src={`https://openweathermap.org/img/wn/${item.weather[0].icon}@2x.png`}
                alt=""
              />
              <div className="temperature">
                {((item.main.temp_max + item.main.temp_min) / 2).toFixed(0) +
                  "°"}
              </div>
              <div className="text">
                {item.weather.map(
                  (item: weather) =>
                    item.description[0].toUpperCase() +
                    item.description.substring(1)
                )}
              </div>
            </div>
          ))}
        </div>
      )}

      {errors.map((e, index) => (
        <div key={index} className="error">
          {e}
        </div>
      ))}
    </div>
  );
}
