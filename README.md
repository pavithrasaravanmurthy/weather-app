# Weather App
![alt screenshot 1](https://gitlab.com/pavithrasaravanmurthy/weather-app/-/raw/master/public/images/Wethaer%20App%201.png)
## Run this project in Local environment
### Download and Open the project in Command terminal, then run the following commands

1. Install the dependencies
```
npm install
```

2. Run app in local and open http://localhost:3000 in browser
 ```
 npm start
 ```


